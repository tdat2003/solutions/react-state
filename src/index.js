// @flow
/* eslint eqeqeq: "off" */

import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { HashRouter, Route } from 'react-router-dom';
import { Alert, Card, NavBar, ListGroup, Row, Column, Button, Form } from './widgets';

import createHashHistory from 'history/createHashHistory';
const history = createHashHistory(); // Use history.push(...) to programmatically change path, for instance after successfully saving a student

class Student {
  id: number;
  static nextId = 1;

  firstName: string;
  lastName: string;
  email: string;

  constructor(firstName: string, lastName: string, email: string) {
    this.id = Student.nextId++;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
  }
}
let students = [
  new Student('Ola', 'Jensen', 'ola.jensen@ntnu.no'),
  new Student('Kari', 'Larsen', 'kari.larsen@ntnu.no')
];

class Course {
  id: number;
  static nextId = 1;

  code: string;
  title: string;

  constructor(code: string, title: string) {
    this.id = Course.nextId++;
    this.code = code;
    this.title = title;
  }
}
let courses = [
  new Course('TDAT3019', 'Systemutvikling 3'),
  new Course('TDAT3020', 'Sikkerhet i programvare og nettverk'),
  new Course('TDAT3022', 'Systemutviklingsprosjekt'),
  new Course('TDAT3023', '3D-grafikk med prosjekt'),
  new Course('TDAT3024', 'Matematikk og fysikk valgfag'),
  new Course('TDAT3025', 'Anvendt maskinlæring med prosjekt')
];

class CourseRegistration {
  courseId: number;
  studentId: number;

  constructor(courseId: number, studentId: number) {
    this.courseId = courseId;
    this.studentId = studentId;
  }
}
let courseRegistrations = [new CourseRegistration(1, 1), new CourseRegistration(6, 1), new CourseRegistration(6, 2)];

class Menu extends Component {
  render() {
    return (
      <NavBar>
        <NavBar.Brand>React example</NavBar.Brand>
        <NavBar.Link to="/students">Students</NavBar.Link>
        <NavBar.Link to="/courses">Courses</NavBar.Link>
      </NavBar>
    );
  }
}

class Home extends Component {
  render() {
    return (
      <Card title="React example with component state">Client-server communication will be covered next week.</Card>
    );
  }
}

class StudentList extends Component {
  render() {
    return (
      <Card title="Students">
        <ListGroup>
          {students.map(student => (
            <ListGroup.Item key={student.email} to={'/students/' + student.id}>
              {student.firstName} {student.lastName}
            </ListGroup.Item>
          ))}
        </ListGroup>
        <Button.Success onClick={() => history.push('/students/new')}>New student</Button.Success>
      </Card>
    );
  }
}

class StudentNew extends Component {
  email = '';
  firstName = '';
  lastName = '';
  form = null;

  render() {
    return (
      <Card title="New student">
        <form ref={e => (this.form = e)}>
          <Form.Input
            type="text"
            label="First name"
            value={this.firstName}
            onChange={event => (this.firstName = event.target.value)}
            required
          />
          <Form.Input
            type="text"
            label="Last name"
            value={this.lastName}
            onChange={event => (this.lastName = event.target.value)}
            required
          />
          <Form.Input
            type="text"
            label="Email"
            value={this.email}
            onChange={event => (this.email = event.target.value)}
            required
            pattern=".*@.*"
          />
          <Button.Success onClick={this.create}>Create student</Button.Success>
          <Button.Light onClick={() => history.push('/students')}>Cancel</Button.Light>
        </form>
      </Card>
    );
  }

  create() {
    if (!this.form || !this.form.checkValidity()) return;
    let new_id = students.push(new Student(this.firstName, this.lastName, this.email));
    history.push('/students/' + new_id);
  }
}

class StudentDetails extends Component<{ match: { params: { id: number } } }> {
  render() {
    let student = students.find(student => student.id == this.props.match.params.id);
    if (!student) {
      Alert.danger('Student not found: ' + this.props.match.params.id);
      return null;
    }
    return (
      <Card title="Details">
        <ListGroup>
          <ListGroup.Item>First name: {student.firstName}</ListGroup.Item>
          <ListGroup.Item>Last name: {student.lastName}</ListGroup.Item>
          <ListGroup.Item>Email: {student.email}</ListGroup.Item>
        </ListGroup>
        <Card title="Courses">
          <ListGroup>
            {courseRegistrations.filter(registration => registration.studentId == student.id).map(registration => {
              let course = courses.find(course => course.id == registration.courseId);
              if (!course) return null;
              return <ListGroup.Item key={course.code}>{course.title}</ListGroup.Item>;
            })}
          </ListGroup>
        </Card>
        <Row>
          <Column width={11}>
            <Button.Danger onClick={() => history.push('/students/' + student.id + '/edit')}>Edit</Button.Danger>
          </Column>
          <Column width={1}>
            <Button.Danger onClick={this.remove}>Remove</Button.Danger>
          </Column>
        </Row>
      </Card>
    );
  }

  remove() {
    students = students.filter(student => student.id != this.props.match.params.id);
    courseRegistrations = courseRegistrations.filter(
      registration => registration.studentId != this.props.match.params.id
    );
    history.push('/students');
  }
}

class StudentEdit extends Component<{ match: { params: { id: number } } }> {
  firstName = ''; // Always initialize component member variables
  lastName = '';
  email = '';
  courseRegistrations = [];
  form = null;

  render() {
    let participatingCourses = this.courseRegistrations.reduce((participatingCourses, registration) => {
      if (registration.studentId == this.props.match.params.id) {
        let course = courses.find(course => course.id == registration.courseId);
        if (course) participatingCourses.push(course);
      }
      return participatingCourses;
    }, []);
    let nonParticipatingCourses = courses.filter(
      course => !participatingCourses.some(participatingCourse => course.code == participatingCourse.code)
    );

    return (
      <Card title="Edit">
        <form ref={e => (this.form = e)}>
          <Form.Input
            type="text"
            label="First name"
            value={this.firstName}
            onChange={event => (this.firstName = event.target.value)}
            required
          />
          <Form.Input
            type="text"
            label="Last name"
            value={this.lastName}
            onChange={event => (this.lastName = event.target.value)}
            required
          />
          <Form.Input
            type="text"
            label="Email"
            value={this.email}
            onChange={event => (this.email = event.target.value)}
            required
            pattern=".*@.*"
          />
          <Card title="Courses">
            <b>Registered</b>
            <ListGroup>
              {participatingCourses.map(course => (
                <ListGroup.Item key={course.code}>
                  <Row>
                    <Column width={4}>{course.code + ' ' + course.title}</Column>
                    <Column width={8}>
                      <Button.Danger
                        onClick={() =>
                          (this.courseRegistrations = this.courseRegistrations.filter(
                            registration =>
                              registration.studentId != this.props.match.params.id || registration.courseId != course.id
                          ))
                        }
                      >
                        -
                      </Button.Danger>
                    </Column>
                  </Row>
                </ListGroup.Item>
              ))}
            </ListGroup>
            <b>Not registered</b>
            <ListGroup>
              {nonParticipatingCourses.map(course => (
                <ListGroup.Item key={course.code}>
                  <Row>
                    <Column width={4}>{course.code + ' ' + course.title}</Column>
                    <Column width={8}>
                      <Button.Success
                        onClick={() =>
                          this.courseRegistrations.push(new CourseRegistration(course.id, this.props.match.params.id))
                        }
                      >
                        +
                      </Button.Success>
                    </Column>
                  </Row>
                </ListGroup.Item>
              ))}
            </ListGroup>
          </Card>
          <Button.Danger onClick={this.save}>Save</Button.Danger>
          <Button.Light onClick={() => history.push('/students/' + this.props.match.params.id)}>Cancel</Button.Light>
        </form>
      </Card>
    );
  }

  // Initialize component state (firstName, lastName, email) when the component has been inserted into the DOM (mounted)
  mounted() {
    let student = students.find(student => student.id == this.props.match.params.id);
    if (!student) {
      Alert.danger('Student not found: ' + this.props.match.params.id);
      return;
    }

    this.firstName = student.firstName;
    this.lastName = student.lastName;
    this.email = student.email;
    this.courseRegistrations = courseRegistrations.slice(); // Make copy
  }

  save() {
    if (!this.form || !this.form.checkValidity()) return;

    let student = students.find(student => student.id == this.props.match.params.id);
    if (!student) {
      Alert.danger('Student not found: ' + this.props.match.params.id);
      return;
    }

    student.firstName = this.firstName;
    student.lastName = this.lastName;
    student.email = this.email;
    courseRegistrations = this.courseRegistrations;

    // Go to StudentDetails after successful save
    history.push('/students/' + student.id);
  }
}

class CourseList extends Component {
  render() {
    return (
      <Card title="Courses">
        <ListGroup>
          {courses.map(course => (
            <ListGroup.Item key={course.code} to={'/courses/' + course.id}>
              {course.title}
            </ListGroup.Item>
          ))}
        </ListGroup>
        <Button.Success onClick={() => history.push('/courses/new')}>New course</Button.Success>
      </Card>
    );
  }
}

class CourseNew extends Component {
  code = '';
  title = '';
  form = null;

  render() {
    return (
      <Card title="New course">
        <form ref={e => (this.form = e)}>
          <Form.Input
            type="text"
            label="Code"
            value={this.code}
            onChange={event => (this.code = event.target.value)}
            required
            pattern=".*[0-9]+"
          />
          <Form.Input
            type="text"
            label="Title"
            value={this.title}
            onChange={event => (this.title = event.target.value)}
            required
          />
          <Button.Success onClick={this.create}>Create course</Button.Success>
          <Button.Light onClick={() => history.push('/courses')}>Cancel</Button.Light>
        </form>
      </Card>
    );
  }

  create() {
    if (!this.form || !this.form.checkValidity()) return;
    let new_id = courses.push(new Course(this.code, this.title));
    history.push('/courses/' + new_id);
  }
}

class CourseDetails extends Component<{ match: { params: { id: number } } }> {
  render() {
    let course = courses.find(course => course.id == this.props.match.params.id);
    if (!course) {
      Alert.danger('Course not found: ' + this.props.match.params.id);
      return null;
    }
    return (
      <Card title="Details">
        <ListGroup>
          <ListGroup.Item>Code: {course.code}</ListGroup.Item>
          <ListGroup.Item>Title: {course.title}</ListGroup.Item>
        </ListGroup>
        <Card title="Students">
          <ListGroup>
            {courseRegistrations.filter(registration => registration.courseId == course.id).map(registration => {
              let student = students.find(student => student.id == registration.studentId);
              if (!student) return null;
              return (
                <ListGroup.Item key={student.email}>
                  {student.firstName} {student.lastName}
                </ListGroup.Item>
              );
            })}
          </ListGroup>
        </Card>
        <Row>
          <Column width={11}>
            <Button.Danger onClick={() => history.push('/courses/' + course.id + '/edit')}>Edit</Button.Danger>
          </Column>
          <Column width={1}>
            <Button.Danger onClick={this.remove}>Remove</Button.Danger>
          </Column>
        </Row>
      </Card>
    );
  }

  remove() {
    courses = courses.filter(course => course.id != this.props.match.params.id);
    courseRegistrations = courseRegistrations.filter(
      registration => registration.courseId != this.props.match.params.id
    );
    history.push('/courses');
  }
}

class CourseEdit extends Component<{ match: { params: { id: number } } }> {
  code = ''; // Always initialize component member variables
  title = '';
  courseRegistrations = [];
  form = null;

  render() {
    let participatingStudents = this.courseRegistrations.reduce((participatingStudents, registration) => {
      if (registration.courseId == this.props.match.params.id) {
        let student = students.find(student => student.id == registration.studentId);
        if (student) participatingStudents.push(student);
      }
      return participatingStudents;
    }, []);
    let nonParticipatingStudents = students.filter(
      student => !participatingStudents.some(participatingStudent => student.email == participatingStudent.email)
    );

    return (
      <Card title="Edit">
        <form ref={e => (this.form = e)}>
          <Form.Input
            type="text"
            label="Code"
            value={this.code}
            onChange={event => (this.code = event.target.value)}
            required
            pattern=".*[0-9]+"
          />
          <Form.Input
            type="text"
            label="Title"
            value={this.title}
            onChange={event => (this.title = event.target.value)}
            required
          />
          <Card title="Students">
            <b>Registered</b>
            <ListGroup>
              {participatingStudents.map(student => (
                <ListGroup.Item key={student.email}>
                  <Row>
                    <Column width={4}>{student.firstName + ' ' + student.lastName}</Column>
                    <Column width={8}>
                      <Button.Danger
                        onClick={() =>
                          (this.courseRegistrations = this.courseRegistrations.filter(
                            registration =>
                              registration.studentId != student.id ||
                              registration.courseId != this.props.match.params.id
                          ))
                        }
                      >
                        -
                      </Button.Danger>
                    </Column>
                  </Row>
                </ListGroup.Item>
              ))}
            </ListGroup>
            <b>Not registered</b>
            <ListGroup>
              {nonParticipatingStudents.map(student => (
                <ListGroup.Item key={student.email}>
                  <Row>
                    <Column width={4}>{student.firstName + ' ' + student.lastName}</Column>
                    <Column width={8}>
                      <Button.Success
                        onClick={() =>
                          this.courseRegistrations.push(new CourseRegistration(this.props.match.params.id, student.id))
                        }
                      >
                        +
                      </Button.Success>
                    </Column>
                  </Row>
                </ListGroup.Item>
              ))}
            </ListGroup>
          </Card>
          <Button.Danger onClick={this.save}>Save</Button.Danger>
          <Button.Light onClick={() => history.push('/courses/' + this.props.match.params.id)}>Cancel</Button.Light>
        </form>
      </Card>
    );
  }

  // Initialize component state (firstName, lastName, email) when the component has been inserted into the DOM (mounted)
  mounted() {
    let course = courses.find(course => course.id == this.props.match.params.id);
    if (!course) {
      Alert.danger('Course not found: ' + this.props.match.params.id);
      return;
    }

    this.code = course.code;
    this.title = course.title;
    this.courseRegistrations = courseRegistrations.slice(); // Make copy
  }

  save() {
    if (!this.form || !this.form.checkValidity()) return;

    let course = courses.find(course => course.id == this.props.match.params.id);
    if (!course) {
      Alert.danger('Course not found: ' + this.props.match.params.id);
      return;
    }

    course.code = this.code;
    course.title = this.title;
    courseRegistrations = this.courseRegistrations;

    // Go to StudentDetails after successful save
    history.push('/courses/' + course.id);
  }
}

const root = document.getElementById('root');
if (root)
  ReactDOM.render(
    <HashRouter>
      <div>
        <Alert />
        <Menu />
        <Route exact path="/" component={Home} />
        <Route path="/students" component={StudentList} />
        <Route exact path="/students/new" component={StudentNew} />
        <Route exact path="/students/:id(\d+)" component={StudentDetails} />
        <Route exact path="/students/:id(\d+)/edit" component={StudentEdit} />
        <Route path="/courses" component={CourseList} />
        <Route exact path="/courses/new" component={CourseNew} />
        <Route exact path="/courses/:id(\d+)" component={CourseDetails} />
        <Route exact path="/courses/:id(\d+)/edit" component={CourseEdit} />
      </div>
    </HashRouter>,
    root
  );
